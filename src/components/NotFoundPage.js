import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => {
  return (
    <div>
      PAGE NOT FOUND - 404! <Link to="/">Home</Link>
    </div>
  );
};

export default NotFoundPage;