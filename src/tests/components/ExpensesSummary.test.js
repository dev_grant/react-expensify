import React from 'react';
import { shallow } from 'enzyme';
import { ExpensesSummary } from '../../components/ExpensesSummary';

test('should render ExpensesSummary component', () => {
  const wrapper = shallow(<ExpensesSummary expenseCount={1} expensesTotal={234} />);
  expect(wrapper).toMatchSnapshot();
});

test('should render ExpensesSummary component', () => {
  const wrapper = shallow(<ExpensesSummary expenseCount={4} expensesTotal={23545454} />);
  expect(wrapper).toMatchSnapshot();
});
