import selectExpensesTotal from '../../selectors/expenses-total';
import { expenses } from '../fixtures/expenses';

test('should return 0 if expense array is empty', () => {
  const total = 0;
  const result = selectExpensesTotal([]);
  expect(result).toBe(total);
});

test('should correctly add up a single expense', () => {
  const total = expenses[0].amount;
  const result = selectExpensesTotal([expenses[0]]);
  expect(result).toBe(total);
});

test('should correctly add up multiple expenses', () => {
  const total = expenses[0].amount + expenses[1].amount + expenses[2].amount;
  const result = selectExpensesTotal(expenses);
  expect(result).toBe(5790);
});


